var express = require('express'); //referencia al paquete express
var app= express(); //paquete express nos va permitir hacer peticiones http
var userFile = require('./user.json');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port=process.env.PORT || 3000;
const URL_BASE = '/api-peru/v1/';

//operaciòn GET del 'Hola mundo'ademas todos los usuarios (users.json)
app.get (URL_BASE,
    function(request,response){
        response.send('HOLA');
    });


//COLLECTIONH

app.get (URL_BASE+'users',
    function(request,response){
        response.status(200);
        response.send(userFile);
    });

//operation get a un usuario con id (instance) 'users/:id/:id2/:id3'
app.get (URL_BASE+'users/:id/:id2',
    function(request,response){
//variable local let
      let indice = request.params.id;
      let indice2 = request.params.id2;
      console.log(indice);
      console.log(indice2);
      response.status(200);
      response.send(userFile[indice-1]);
      response.send(userFile[indice2-2]);
    });

    //operation get a un usuario con id (instance) 'users/:id'
    app.get (URL_BASE+'users/:id',
        function(request,response){
    //variable local let
          let indice = request.params.id;
          let repuesta =
          (userFile[indice-1] == undefined) ? {"msg": "No existe"} : userFile[indice-1];
          //(userFile[indice-1] == undefined) ? {"msg":"No existe"} : userFile[indice-1]
          response.status(200);
          response.send(repuesta);
        });

//PETICION GET CON QUERY STRING

app.get (URL_BASE+'usersq',
    function(request,response){
      console.log(request.query);
      let query = request.query
      var datos = []
      console.log("maximo:"+query.max);
      for (var i = 0; i < query.max; i++) {
        datos.push(query.max-i);
      }
      console.log(datos);
      response.send(200);
    });


// peticion GET todos los usuarios
  app.get (URL_BASE+'userstotal',
      function(request,response){
        console.log(request.query);
        let total = userFile.length;
        let totalJason = JSON.stringify({numero_elementos:total});
        //reponse.status(200).send(totalJason);
        response.status(200).send({"total": total});
      });

      //operacion POST

      // Operación POST a users
      app.post(URL_BASE + 'users',
          function(req, res) {
            //console.log(req.body.id);
            //console.log(req.body);
            let total = userFile.length;
            let newID = parseInt(total)+1;
            let newUser={
              id: newID,
              first_name: req.body.first_name,
              last_name: req.body.last_name,
              email: req.body.email,
              password: req.body.password
            };

          userFile.push(newUser);
          res.status(201);
          res.send({"mensaje":"Usuario creado corectamente",
          "usuario":newUser,
          "userFile actualizado":userFile})

          });

          // Operación POST a users  --PUT
                    app.post(URL_BASE + 'usersput',
              function(req, res) {
                let newUser={
                  id: req.body.id,
                  first_name: req.body.first_name,
                  last_name: req.body.last_name,
                  email: req.body.email,
                  password: req.body.password
                };

              userFile.put(req.body.id);
              res.status(201);
              res.send({"mensaje":"Usuario actualizado corectamente",
              "usuario":newUser,
              "userFile actualizado":userFile})

              });

              // Operación delete a users  --PUT
                        app.delete(URL_BASE + 'users/:id',
                  function(request, response) {
                    let indice = request.params.id;
                    let repuesta =
                    (userFile[indice-1] == undefined) ? {"msg": "No existe"} :
                    { "Usuario Eliminado" : (userFile.splice(indice-1,1))};
                    //(userFile[indice-1] == undefined) ? {"msg":"No existe"} : userFile[indice-1]
                    response.status(200);
                    response.send(repuesta);


                  });

//servidor escuchara en la url (servidor local)
//con control c para parar el servidor




app.listen(port,function() {
  console.log('NODE js ESCUCHANDO EN EL PUERTO 3000')
}); //funcion anonima
